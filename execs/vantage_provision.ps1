#Provision Script
Set-TimeZone -Name "Eastern Standard Time"

DISM /Online /Enable-Feature /FeatureName:NetFx3


######## Install Silverlight ##########
SilverLight.exe /q /norestart



#################################################### Get chrome #######################################
$Path = $env:TEMP; $Installer = "chrome_installer.exe"; Invoke-WebRequest "http://dl.google.com/chrome/install/375.126/chrome_installer.exe" -OutFile $Path\$Installer; Start-Process -FilePath $Path\$Installer -Args "/silent /install" -Verb RunAs -Wait; Remove-Item $Path\$Installer



###################### Get 7 zip #########################
# Silent Install 7-Zip
# http://www.7-zip.org/download.html 

# Path for the workdir
$workdir = "c:\installer\"

# Check if work directory exists if not create it

If (Test-Path -Path $workdir -PathType Container)
{ Write-Host "$workdir already exists" -ForegroundColor Red }
ELSE
{ New-Item -Path $workdir  -ItemType directory }

# Download the installer

$source = "http://www.7-zip.org/a/7z1900-x64.msi"
$destination = "$workdir\7-Zip.msi"

# Check if Invoke-Webrequest exists otherwise execute WebClient

if (Get-Command 'Invoke-Webrequest') {
    Invoke-WebRequest $source -OutFile $destination
}
else {
    $WebClient = New-Object System.Net.WebClient
    $webclient.DownloadFile($source, $destination)
}

Invoke-WebRequest $source -OutFile $destination 

# Start the installation

msiexec.exe /i "$workdir\7-Zip.msi" /qn

# Wait XX Seconds for the installation to finish

Start-Sleep -s 35

# Remove the installer

rm -Force $workdir\7*


################################################ Uninstall Office Retail ###############################################

./o365/setup.exe /configure uninstall-Office365ProPlus.xml


################################################# Silent install Adobe Reader DC########################################
# https://get.adobe.com/nl/reader/enterprise/

# Path for the workdir
$workdir = "c:\installer\"

# Check if work directory exists if not create it

If (Test-Path -Path $workdir -PathType Container)
{ Write-Host "$workdir already exists" -ForegroundColor Red }
ELSE
{ New-Item -Path $workdir  -ItemType directory }

# Download the installer

$source = "http://ardownload.adobe.com/pub/adobe/reader/win/AcrobatDC/1502320053/AcroRdrDC1502320053_en_US.exe"
$destination = "$workdir\adobeDC.exe"

# Check if Invoke-Webrequest exists otherwise execute WebClient

if (Get-Command 'Invoke-Webrequest') {
    Invoke-WebRequest $source -OutFile $destination
}
else {
    $WebClient = New-Object System.Net.WebClient
    $webclient.DownloadFile($source, $destination)
}

# Start the installation

Start-Process -FilePath "$workdir\adobeDC.exe" -ArgumentList "/sPB /rs"

# Wait XX Seconds for the installation to finish

Start-Sleep -s 35

# Remove the installer

rm -Force $workdir\adobe*